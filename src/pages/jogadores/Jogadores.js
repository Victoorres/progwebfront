import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import React, { Component } from 'react';

import TopBar from '../../components/topbar/TopBar';
import SideBar from '../../components/sidebar/SideBar';

import { listarJogadores, excluirJogador } from '../../actions/jogadores/actions';

function mapStateToProps(store) {
    return {
        store,
        error: store.jogadores.error,
        jogadores: store.jogadores.dados,
        loading: store.jogadores.loading
    }
}

class Jogadores extends Component {

    componentDidMount() {
        this.props.dispatch(listarJogadores())
    }

    deletePlayer(id) {
        this.props.dispatch(excluirJogador(id));
    }

    render() {
        const { error, jogadores, loading } = this.props;

        if (loading) return <div>Carregando...</div>

        if (error) return <div>Erro na requisição</div>
        if (jogadores.length > 0) {
            return (
                <div className="container">
                    <TopBar />
                    <div className="row">
                        <div className="col-3">
                            <SideBar />
                        </div>
                        <div className="col-9">
                            <h1>Jogadores</h1>
                            <h4>Quantidade de jogadores: {jogadores.length}</h4>
                            <Link className="btn btn-primary btn-large mb-2" to="/jogadores/novo">Novo Jogador</Link>
                            <table className="table table-dark">
                                <thead>
                                    <tr>
                                        <th scope="col">#</th>
                                        <th scope="col">Nome</th>
                                        <th scope="col">País</th>
                                        <th scope="col">Time</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    {
                                        jogadores.map((jogador) => (
                                            <tr key={jogador.id}>
                                                <th scope="row">{jogador.id}</th>
                                                <td>{jogador.name}</td>
                                                <td>{jogador.country}</td>
                                                <td>{jogador.team_name}</td>
                                                <td>
                                                    <Link className="btn btn-info mr-2" to={{ pathname: `/jogadores/${jogador.id}` }}>Detalhes</Link>
                                                    <Link className="btn btn-warning mr-2" to={{ pathname: `/jogadores/editar/${jogador.id}` }}>Editar</Link>
                                                    <button className="btn btn-danger" onClick={() => this.deletePlayer(jogador.id)}>Excluir</button>
                                                </td>
                                            </tr>
                                        ))
                                    }
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            );
        } else {
            return (
                <div className="container">
                    <TopBar />
                    <div className="row">
                        <div className="col-3">
                            <SideBar />
                        </div>
                        <div className="col-9">
                            <h1>Jogadores</h1>
                            <h4>Quantidade de jogadores: {jogadores.length}</h4>
                            <Link className="btn btn-primary btn-large mb-2" to="/jogadores/novo">Novo Jogador</Link>
                            <table className="table table-dark">
                                <thead>
                                    <tr>
                                        <th scope="col">#</th>
                                        <th scope="col">Nome</th>
                                        <th scope="col">País</th>
                                        <th scope="col">Time</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <td colSpan={4} style={{ textAlign: 'center' }}>Não há jogadores cadastrados!</td>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            );
        }
    }
}

export default connect(mapStateToProps)(Jogadores);