import { connect } from 'react-redux';
import React, { Component } from 'react';

import TopBar from '../../components/topbar/TopBar';
import SideBar from '../../components/sidebar/SideBar';
import { detalhesJogador } from '../../actions/jogadores/actions';

function mapStateToProps(store) {
    return {
        error: store.jogadores.error,
        jogador: store.jogadores.jogador,
        loading: store.jogadores.loading
    }
}

class InfoJogador extends Component {
    
    async componentDidMount() {
        await this.props.dispatch(detalhesJogador(this.props.match.params.id));
    }

    render() {
        let { jogador, loading, error } = this.props;

        if (loading) return <div>Carregando...</div>;

        if (error) {
            if (error.request.status === 404) {
                return <div>Jogador não encontrado...</div>
            }
            return <div>Erro na requisição</div>
        }

        return (
            <div className="container">
                <TopBar />
                <div className="row">
                    <div className="col-3">
                        <SideBar />
                    </div>
                    <div className="col-9">
                        <h1>Dados do Jogador</h1>
                        <h4>{jogador.name}</h4>
                    </div>
                </div>
            </div>
        )
    }
}

export default connect(mapStateToProps)(InfoJogador);