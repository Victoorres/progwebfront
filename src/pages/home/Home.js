import React, { Component } from 'react';

import TopBar from '../../components/topbar/TopBar';
import SideBar from '../../components/sidebar/SideBar';

class Home extends Component {
    render() {
        return (
            <div className="container">
                <TopBar />
                <div className="row">
                    <div className="col-3">
                        <SideBar />
                    </div>
                    <div className="col-9">
                        <h1>Home Page</h1>
                    </div>
                </div>
            </div>
        );
    }
};

export default Home;