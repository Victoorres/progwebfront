import React, { Component } from 'react';
import { connect } from 'react-redux';
import {
    Field,
    reduxForm
} from 'redux-form';

import TopBar from '../../components/topbar/TopBar';
import SideBar from '../../components/sidebar/SideBar';

import { gravarJogador, detalhesJogador, cleanPlayer } from './../../actions/jogadores/actions';

class NovoJogador extends Component {

    title = "Novo Jogador";

    componentDidMount() {
        if (this.props.match.params.id) {
            this.title = "Editar Jogador";
            this.props.dispatch(detalhesJogador(this.props.match.params.id));
        } else {
            this.props.dispatch(cleanPlayer());
        }
    }

    submit(dados, gravarJogador) {
        this.props.dispatch(gravarJogador(dados));
        return window.location.href = "http://localhost:3000/jogadores";
    }

    render() {

        const { handleSubmit } = this.props;

        return (
            <div className="container">
                <TopBar />
                <div className="row">
                    <div className="col-3">
                        <SideBar />
                    </div>
                    <div className="col-9">
                        <h1>{this.title}</h1>
                        <form onSubmit={handleSubmit((fields) => this.submit(fields, gravarJogador))}>
                            <label>Nome</label>
                            <Field component="input" name="name" className="form-control" placeholder="Informe o nome" required="true" />
                            <label>Nacionalidade</label>
                            <Field component="input" name="country" className="form-control" placeholder="Informe a nacionalidade" required="true" />
                            <label>Biografia</label>
                            <Field component="textarea" name="bio" className="form-control" placeholder="Insira uma breve biografia" required="true" />
                            <label>Ano nascimento</label>
                            <Field component="input" type="number" name="nascimento" className="form-control" placeholder="Informe somente o ano de nascimento" required="true" />
                            <label>Time</label>
                            <Field component="input" name="team_name" className="form-control" placeholder="Em qual time está jogando?" required="true" />
                            <label>País do Time</label>
                            <Field component="input" name="team_country" className="form-control" placeholder="Informe o país do time" required="true" />

                            <button type="submit" className="btn btn-primary mt-2">
                                Salvar
                            </button>
                        </form>
                    </div>
                </div>
            </div>
        )
    }
    returnHome(submit) {
        if (submit.valid) {
            this.document.URL = '/'
        }
    }
}

NovoJogador = reduxForm({
    form: 'jogador',
    enableReinitialize: true
})(NovoJogador);


NovoJogador = connect(state => ({
    initialValues: state.jogadores.jogador
}), { load: detalhesJogador })(NovoJogador);

export default NovoJogador;