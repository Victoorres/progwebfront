import * as TYPES from './../actions/jogadores/types';

const initialState = {
    dados: [],
    jogador: {},
    error: null,
    loading: false
}

export default (state = initialState, action) => {
    switch (action.type) {
        case TYPES.INICIAR_BUSCA:
            return {
                ...state,
                loading: true,
                error: false
            };
        case TYPES.SUCESSO_BUSCA:
            return {
                ...state,
                loading: false,
                dados: action.payload.jogadores
            };
        case TYPES.FALHA_BUSCA:
            return {
                ...state,
                loading: false,
                error: action.payload.error,
                dados: []
            };
        case TYPES.SUCESSO_BUSCAR_JOGADOR:
            return {
                ...state,
                loading: false,
                jogador: action.payload.jogador
            }
        case TYPES.FALHA_BUSCAR_JOGADOR:
            return {
                ...state,
                loading: false,
                jogador: {},
                error: action.payload.error
            }
        default:
            return state;
    }
}