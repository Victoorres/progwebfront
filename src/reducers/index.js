import { combineReducers } from 'redux';
import jogadores from './jogadores';
import { reducer as formReducer } from 'redux-form';

const Reducers = combineReducers({
    jogadores,
    form: formReducer
});

export default Reducers;