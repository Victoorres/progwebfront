import React, { Component } from 'react';
import { Link } from 'react-router-dom';

import './sidebar.css';

class SideBar extends Component {
    render() {
        return (
            <ul className="nav flex-column">
                <li className="nav-item">
                    <Link to="/" className="sidebar-btn btn btn-outline-primary btn-lg mb-2" >Home</Link>
                </li>
                <li className="nav-item">
                    <Link to="/jogadores" className="sidebar-btn btn btn-outline-primary btn-lg">Jogadores</Link>
                </li>
            </ul>
        );
    }
}

export default SideBar;