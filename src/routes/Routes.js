import React from 'react';
import {
    BrowserRouter,
    Switch,
    Route
} from 'react-router-dom';

import Home from '../pages/home/Home';
import Jogadores from '../pages/jogadores/Jogadores';
import InfoJogador from '../pages/infoJogador/InfoJogador';
import NovoJogador from '../pages/NovoJogador/NovoJogador';

const Routes = () => {
    return (
        <BrowserRouter>
            <Switch>
                <Route exact path="/" component={ Home }/>
                <Route exact path="/jogadores" component={ Jogadores }/>
                <Route exact path="/jogadores/novo" component={ NovoJogador }/>
                <Route exact path="/jogadores/editar/:id" component={ NovoJogador }/>
                <Route exact path="/jogadores/:id" component={ InfoJogador }/>
            </Switch>
        </BrowserRouter>
    );
}

export default Routes;