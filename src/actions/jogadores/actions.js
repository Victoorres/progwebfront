import * as TYPES from './types';

import api from './../../services/api';

export const iniciarBusca = () => {
    return {
        type: TYPES.INICIAR_BUSCA
    }
}

export const sucessoBusca = (jogadores) => {
    return {
        type: TYPES.SUCESSO_BUSCA,
        payload: { jogadores }
    }
}

export const falhaBusca = (error) => {
    return {
        type: TYPES.FALHA_BUSCA,
        payload: { error }
    }
}

export const sucessoBuscaJogador = (jogador) => {
    return {
        type: TYPES.SUCESSO_BUSCAR_JOGADOR,
        payload: { jogador }
    }
}

export const falhaBuscaJogador = (error) => {
    return {
        type: TYPES.FALHA_BUSCAR_JOGADOR,
        payload: { error }
    }
}

export function listarJogadores() {
    return dispatch => {
        dispatch(iniciarBusca());
        api.get('/jogadores').then(response => {
            dispatch(sucessoBusca(response.data))
        }).catch(error => {
            dispatch(falhaBusca(error))
        })
    }
}

export function detalhesJogador(id) {
    return (dispatch) => {
        dispatch(iniciarBusca());
          api.get(`/jogadores/${id}`).then(response => {
            dispatch(sucessoBuscaJogador(response.data))
        }).catch(error => {
            dispatch(falhaBuscaJogador(error))
        })
    }
}

export function gravarJogador(dados) {
    return dispatch => {
        dispatch(iniciarBusca());
        api.post('/jogadores', dados).then(response => {
            dispatch(sucessoBuscaJogador(response.data))
        }).catch((error) => {
            dispatch(falhaBuscaJogador(error));
        })
    }
}

export function excluirJogador(id) {
    return dispatch => {
        dispatch(iniciarBusca());
        api.delete(`/jogadores/${id}`).then(response => {
            dispatch(sucessoBusca(response.data));
        }).catch((error) => {
            console.log(error);
        })
    }
}

export function cleanPlayer() {
    return dispatch => {
        dispatch(sucessoBuscaJogador({}));
    }
}